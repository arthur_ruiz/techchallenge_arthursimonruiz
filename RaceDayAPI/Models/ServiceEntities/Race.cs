﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace RaceDayAPI.Models.ServiceEntities
{
    public class Race
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("start")]
        public DateTime Start { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("horses")]
        public List<Horse> Horses { get; set; }
    }
}
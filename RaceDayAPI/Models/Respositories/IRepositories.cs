﻿namespace RaceDayAPI.Models.Respositories
{
    public interface IRepositories
    {
        ICustomerRepository CustomerRepository { get; set; }
        IBetRepository BetRepository { get; set; }
        IRaceRepository RaceRepository { get; set; }
    }
}

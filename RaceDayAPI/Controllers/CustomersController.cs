﻿using AutoMapper;
using RaceDayAPI.Models.DTOs;
using RaceDayAPI.Models.Respositories;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace RaceDayAPI.Controllers
{
    public class CustomersController : ApiController
    {

        private IRepositories _repositories;

        public CustomersController()
        {
            _repositories = new Repositories();
        }

        public CustomersController(IRepositories repositories)
        {
            _repositories = repositories;
        }
        

        [Route("customers")]
        public async Task<IEnumerable<CustomerDTO>> GetCustomers()
        {
            var customers = await _repositories.CustomerRepository.GetCustomers();

            return Mapper.Map<IEnumerable<CustomerDTO>>(customers);
        }

        [Route("customers/totalbetamount")]
        public async Task<ResultDecimalDTO> GetCustomersTotalBetAmount()
        {
            var bets = await _repositories.BetRepository.GetBets();

            var amount = bets.Sum(b => b.Stake);

            return new ResultDecimalDTO { Result = amount };
            
        }

        [Route("customers/risky")]
        public async Task<IEnumerable<CustomerDTO>> GetCustomersAtRisk()
        {

            //200
            var riskyValue = int.Parse(ConfigurationManager.AppSettings["riskyValue"].ToString());

            var customersTask = _repositories.CustomerRepository.GetCustomers();
            var betsTask = _repositories.BetRepository.GetBets();

            var customers = await customersTask;
            var bets = await betsTask;

            return from b in bets.Where(b => b.Stake > riskyValue)
                   join custs in customers on b.CustomerId equals custs.Id
                   group custs by custs.Id into custgroup
                   select new CustomerDTO { Id = custgroup.First().Id, Name = custgroup.First().Name };

        }

        [Route("customers/{id}")]
        public async Task<CustomerDTO> GetCustomer(int id)
        {
            var customer = await _repositories.CustomerRepository.GetCustomer(id);

            if (customer == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            return Mapper.Map<CustomerDTO>(customer);
        }

        [Route("customers/{id}/totalbetamount")]
        public async Task<ResultDecimalDTO> GetCustomerTotalBetAmount(int id)
        {
            var customer = await _repositories.CustomerRepository.GetCustomer(id);

            if(customer == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            var bets = await _repositories.BetRepository.GetBets();

            var amount = bets.Where(b => b.CustomerId == id).Sum(b => b.Stake);

            return new ResultDecimalDTO { Result = amount };

        }

        [Route("customers/{id}/totalbets")]
        public async Task<ResultIntDTO> GetCustomerTotalBets(int id)
        {
            var customer = await _repositories.CustomerRepository.GetCustomer(id);

            if (customer == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            var bets = await _repositories.BetRepository.GetBets();

            var amount = bets.Where(b => b.CustomerId == id).Count();

            return new ResultIntDTO { Result = amount };

        }
    }
}

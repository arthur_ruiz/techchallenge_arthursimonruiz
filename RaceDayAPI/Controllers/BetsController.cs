﻿using AutoMapper;
using RaceDayAPI.Models.DTOs;
using RaceDayAPI.Models.Respositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace RaceDayAPI.Controllers
{
    public class BetsController : ApiController
    {
        private IRepositories _repositories;

        public BetsController()
        {
            _repositories = new Repositories();
        }

        public BetsController(IRepositories repositories)
        {
            _repositories = repositories;
        }

        [Route("bets")]
        public async Task<IEnumerable<BetDTO>> GetBets()
        {

            var bets = await _repositories.BetRepository.GetBets();

            return Mapper.Map<IEnumerable<BetDTO>>(bets);

        }
    }
}

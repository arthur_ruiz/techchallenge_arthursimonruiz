﻿using RaceDayAPI.Models.Respositories;
using RaceDayAPI.Models.ServiceEntities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RaceDayAPI.Tests.Models
{
    public class TestRaceRepository : IRaceRepository
    {
        public async Task<IEnumerable<Race>> GetRaces()
        {
            List<Race> races = new List<Race>();
            races.Add(new Race
            {
                Id = 1,
                Name = "R1",
                Start = DateTime.Parse("2017-12-08T02:00:00+00:00"),
                Status = "completed",
                Horses = new List<Horse>()
                {
                    new Horse{ Id=11, Name = "Big Orange", Odds = 5.5M },
                    new Horse{ Id=12, Name = "OUR IVANHOWE", Odds = 1.5M },
                    new Horse{ Id=13, Name = "CURREN MIROTIC", Odds = 3M }
                }
            });

            races.Add(new Race
            {
                Id = 2,
                Name = "R2",
                Start = DateTime.Parse("2017-12-08T03:00:00+00:00"),
                Status = "completed",
                Horses = new List<Horse>()
                {
                    new Horse{ Id=21, Name = "BONDI BEACH", Odds = 5.5M },
                    new Horse{ Id=22, Name = "EXOSPHERIC", Odds = 1.5M },
                    new Horse{ Id=23, Name = "HARTNELL", Odds = 3M }
                }
            });

            races.Add(new Race
            {
                Id = 3,
                Name = "R3",
                Start = DateTime.Parse("2017-12-08T04:00:00+00:00"),
                Status = "completed",
                Horses = new List<Horse>()
                {
                    new Horse{ Id=31, Name = "WHO SHOT THE BARMAN", Odds = 5.5M },
                    new Horse{ Id=32, Name = "WICKLOW BRAVE", Odds = 1.5M },
                    new Horse{ Id=33, Name = "ALMOONQITH", Odds = 3M }
                }
            });

            races.Add(new Race
            {
                Id = 4,
                Name = "R4",
                Start = DateTime.Parse("2017-12-08T05:00:00+00:00"),
                Status = "pending",
                Horses = new List<Horse>()
                {
                    new Horse{ Id=41, Name = "GALLANTE", Odds = 5.5M },
                    new Horse{ Id=42, Name = "GRAND MARSHAL", Odds = 1.5M },
                    new Horse{ Id=43, Name = "JAMEKA", Odds = 3M }
                }
            });

            races.Add(new Race
            {
                Id = 5,
                Name = "R5",
                Start = DateTime.Parse("2017-12-08T06:00:00+00:00"),
                Status = "pending",
                Horses = new List<Horse>()
                {
                    new Horse{ Id=51, Name = "HEARTBREAK CITY", Odds = 5.5M },
                    new Horse{ Id=52, Name = "SIR JOHN HAWKWOOD", Odds = 1.5M },
                    new Horse{ Id=53, Name = "EXCESS KNOWLEDGE", Odds = 13M },
                    new Horse{ Id=54, Name = ">BEAUTIFUL ROMANCE", Odds = 23M },
                    new Horse{ Id=54, Name = "ALMANDIN", Odds = 10M }
                }
            });

            return races;
        }
    }
}

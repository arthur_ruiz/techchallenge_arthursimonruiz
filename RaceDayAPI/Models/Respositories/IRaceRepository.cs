﻿using RaceDayAPI.Models.ServiceEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RaceDayAPI.Models.Respositories
{
    public interface IRaceRepository
    {
        Task<IEnumerable<Race>> GetRaces();
    }
}

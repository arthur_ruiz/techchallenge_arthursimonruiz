﻿namespace RaceDayAPI.Models.Respositories
{
    public class Repositories: IRepositories
    {

        public Repositories()
        {
            CustomerRepository = new CustomerRepository();
            BetRepository = new BetRepository();
            RaceRepository = new RaceRepository();
        }

        public ICustomerRepository CustomerRepository { get; set; }
        public IBetRepository BetRepository { get; set; }
        public IRaceRepository RaceRepository { get; set; }
    }
}
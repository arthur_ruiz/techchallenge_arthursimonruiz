﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RaceDayAPI;
using RaceDayAPI.Controllers;
using RaceDayAPI.Tests.Models;
using RaceDayAPI.Models.Respositories;
using System.Threading.Tasks;

namespace RaceDayAPI.Tests.Controllers
{
    [TestClass]
    public class CustomersControllerTest
    {

        private IRepositories _repositories;
        private CustomersController _controller;

        public CustomersControllerTest()
        {
            TestConfig.Initialize();
            _repositories = new TestRepositories();
            _controller = new CustomersController(_repositories);
        }

        [TestMethod]
        public void TestGetCustomers()
        {
            var customersTask = _controller.GetCustomers();
            customersTask.Wait();
            var customers = customersTask.Result;

            Assert.AreEqual(13, customers.Count());
        }

        [TestMethod]
        public void TestGetCustomer()
        {
            var customerTask = _controller.GetCustomer(1);
            customerTask.Wait();
            var customer = customerTask.Result;

            Assert.AreEqual(1, customer.Id);
            Assert.AreEqual("Rob", customer.Name);
        }

        [TestMethod]
        public void TestGetTotalBetAmount()
        {
            var amountTask = _controller.GetCustomersTotalBetAmount();
            amountTask.Wait();
            var amount = amountTask.Result;

            Assert.AreEqual(4100, amount.Result);
        }

        [TestMethod]
        public void TestGetCustomerTotalBetAmount()
        {
            var amountTask = _controller.GetCustomerTotalBetAmount(1);
            amountTask.Wait();
            var amount = amountTask.Result;

            Assert.AreEqual(700, amount.Result);
        }

        [TestMethod]
        public void TestGetCustomerTotalBets()
        {
            var countTask = _controller.GetCustomerTotalBets(1);
            countTask.Wait();
            var count = countTask.Result;

            Assert.AreEqual(4, count.Result);
        }
    }
}

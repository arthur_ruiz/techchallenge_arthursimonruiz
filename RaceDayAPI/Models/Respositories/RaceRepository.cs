﻿using Newtonsoft.Json;
using RaceDayAPI.Models.ServiceEntities;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace RaceDayAPI.Models.Respositories
{
    public class RaceRepository: IRaceRepository
    {

        private string _raceServiceUrl;
        private HttpClient _client;

        public RaceRepository()
        {
            _raceServiceUrl = ConfigurationManager.AppSettings["raceServiceUrl"];
            _client = new HttpClient();
        }

        public async Task<IEnumerable<Race>> GetRaces()
        {
            IEnumerable<Race> races = null;

            var response = await _client.GetAsync(_raceServiceUrl);

            if (response.IsSuccessStatusCode)
            {
                var resultString = await response.Content.ReadAsStringAsync();
                races = JsonConvert.DeserializeObject<IEnumerable<Race>>(resultString);
            }

            return races;
        }
    }
}
﻿using Newtonsoft.Json;

namespace RaceDayAPI.Models.ServiceEntities
{
    public class Horse
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("odds")]
        public decimal Odds { get; set; }
    }
}
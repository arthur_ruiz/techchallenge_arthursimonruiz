﻿using Newtonsoft.Json;

namespace RaceDayAPI.Models.ServiceEntities
{
    public class Customer
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
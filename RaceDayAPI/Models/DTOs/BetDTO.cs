﻿namespace RaceDayAPI.Models.DTOs
{
    public class BetDTO
    {
        public int CustomerId { get; set; }
        public int RaceId { get; set; }
        public int HorseId { get; set; }
        public decimal Stake { get; set; }
        public bool Won { get; set; }
    }
}
﻿
var Horse = React.createClass({
    render: function () {
        return (
            <tr>
                <td>{this.props.name}</td>
                <td>{this.props.totalbets}</td>
                <td>${this.props.payout.toFixed(2)}</td>
            </tr>
        );
        
    }
});

var Horses = React.createClass({
    render: function () {
        var horses = this.props.data.map(function (horse) {
            return (
                <Horse key={horse.Id} name={horse.Name} totalbets={horse.TotalBets} payout={horse.WinPayout} />
            );
        });
        return (
            <table className="table">
                <thead>
                    <tr>
                        <th width="40%">Horse</th>
                        <th width="30%">Bets</th>
                        <th width="30%">Payout</th>
                    </tr>
                </thead>
                <tbody>
                    {horses}
                </tbody>
            </table>
        );
    }
});

var Race = React.createClass({
    render: function () {
        return (
            <div className="col-sm-12">
                <div className="row">
                    <div className="col-sm-4">
                        <h3>Race: {this.props.name}</h3>
                    </div>
                    <div className="col-sm-4">
                        <h3>Status: {this.props.status}</h3>
                    </div>
                    <div className="col-sm-4">
                        <h3>Total Bets: ${this.props.amount.toFixed(2)}</h3>
                    </div>
                </div>
                <div className="row">
                    <Horses data={this.props.horses} />
                </div>
            </div>
        );
    }
});

var RaceTable = React.createClass({
    getInitialState: function () {
        return { data: [] };
    },
    componentWillMount: function () {
        var xhr = new XMLHttpRequest();
        xhr.open('get', 'api/races', true);
        xhr.onload = function () {
            var data = JSON.parse(xhr.responseText);
            this.setState({ data: data });
            $("#dataLoading").remove();
        }.bind(this);
        xhr.send();
    },
    render: function () {
        var races = this.state.data.map(function (race) {
            return (
                <Race key={race.Id} name={race.Name} status={race.Status} amount={race.TotalBetAmount} horses={race.Horses}/>
            );
        });
        return (
            <div className="row">
                {races}
            </div>
        );
    }
});
ReactDOM.render(
    <RaceTable />,
    document.getElementById('content')
);
﻿namespace RaceDayAPI.Models.DTOs
{
    public class ResultDecimalDTO
    {
        public decimal Result { get; set; }
    }
}
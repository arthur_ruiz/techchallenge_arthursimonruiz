﻿using RaceDayAPI.Models.ServiceEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RaceDayAPI.Models.Respositories
{
    public interface ICustomerRepository
    {
        Task<IEnumerable<Customer>> GetCustomers();

        Task<Customer> GetCustomer(int id);
    }
}

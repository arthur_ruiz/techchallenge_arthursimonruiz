﻿namespace RaceDayAPI.Models.DTOs
{
    public class ResultIntDTO
    {
        public int Result { get; set; }
    }
}
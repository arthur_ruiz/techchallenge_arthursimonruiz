﻿using System;
using System.Collections.Generic;

namespace RaceDayAPI.Models.DTOs
{
    public class RaceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public string Status { get; set; }
        public decimal TotalBetAmount { get; set; }
        public IEnumerable<HorseDTO> Horses { get; set; }
    }
}
﻿using RaceDayAPI.Models.DTOs;
using RaceDayAPI.Models.Respositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace RaceDayAPI.Controllers
{
    public class RaceController : ApiController
    {
        private IRepositories _repositories;

        public RaceController()
        {
            _repositories = new Repositories();
        }

        public RaceController(IRepositories repositories)
        {
            _repositories = repositories;
        }

        [Route("races")]
        public async Task<IEnumerable<RaceDTO>> GetRaces()
        {
            var raceTask = _repositories.RaceRepository.GetRaces();
            var betsTask = _repositories.BetRepository.GetBets();

            var races = await raceTask;
            var bets = await betsTask;


            return races.Select(r =>
            {
                var raceTotalBets = bets.Where(b => b.RaceId == r.Id).Sum(b => b.Stake);

                return new RaceDTO
                {
                    Id = r.Id,
                    Name = r.Name,
                    Start = r.Start,
                    Status = r.Status,
                    TotalBetAmount = raceTotalBets,
                    Horses = r.Horses.Select(h =>
                    {
                        var horseBets = bets.Where(b => b.RaceId == r.Id && b.HorseId == h.Id);
                        var betCount = horseBets.Count();
                        var payout = betCount > 0 ? (horseBets.Sum(hb => hb.Stake) / betCount) * h.Odds : 0;

                        return new HorseDTO
                        {
                            Id = h.Id,
                            Name = h.Name,
                            Odds = h.Odds,
                            TotalBets = betCount,
                            WinPayout = payout
                        };
                    })
                };
            });
        }
    }
}

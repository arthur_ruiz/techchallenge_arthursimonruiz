﻿using Newtonsoft.Json;

namespace RaceDayAPI.Models.ServiceEntities
{
    public class Bet
    {
        [JsonProperty("customerId")]
        public int CustomerId { get; set; }
        [JsonProperty("raceId")]
        public int RaceId { get; set; }
        [JsonProperty("horseId")]
        public int HorseId { get; set; }
        [JsonProperty("stake")]
        public decimal Stake { get; set; }
        [JsonProperty("won")]
        public bool Won { get; set; }
    }
}
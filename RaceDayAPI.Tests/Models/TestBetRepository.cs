﻿using RaceDayAPI.Models.Respositories;
using RaceDayAPI.Models.ServiceEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RaceDayAPI.Tests.Models
{
    public class TestBetRepository : IBetRepository
    {
        public async Task<IEnumerable<Bet>> GetBets()
        {
            List<Bet> bets = new List<Bet>();
            bets.Add(new Bet { CustomerId = 1, HorseId = 11, RaceId = 1, Stake = 100, Won = true });
            bets.Add(new Bet { CustomerId = 1, HorseId = 21, RaceId = 2, Stake = 150, Won = false });
            bets.Add(new Bet { CustomerId = 1, HorseId = 32, RaceId = 3, Stake = 200, Won = false });
            bets.Add(new Bet { CustomerId = 1, HorseId = 42, RaceId = 4, Stake = 250, Won = true });
            bets.Add(new Bet { CustomerId = 2, HorseId = 55, RaceId = 2, Stake = 300, Won = true });
            bets.Add(new Bet { CustomerId = 2, HorseId = 12, RaceId = 1, Stake = 100, Won = true });
            bets.Add(new Bet { CustomerId = 2, HorseId = 22, RaceId = 2, Stake = 150, Won = true });
            bets.Add(new Bet { CustomerId = 2, HorseId = 31, RaceId = 3, Stake = 200, Won = false });
            bets.Add(new Bet { CustomerId = 3, HorseId = 43, RaceId = 4, Stake = 250, Won = false });
            bets.Add(new Bet { CustomerId = 3, HorseId = 53, RaceId = 5, Stake = 300, Won = false });
            bets.Add(new Bet { CustomerId = 3, HorseId = 13, RaceId = 1, Stake = 100, Won = false });
            bets.Add(new Bet { CustomerId = 4, HorseId = 21, RaceId = 2, Stake = 150, Won = true });
            bets.Add(new Bet { CustomerId = 4, HorseId = 31, RaceId = 3, Stake = 200, Won = false });
            bets.Add(new Bet { CustomerId = 4, HorseId = 43, RaceId = 4, Stake = 250, Won = true });
            bets.Add(new Bet { CustomerId = 5, HorseId = 54, RaceId = 5, Stake = 300, Won = false });
            bets.Add(new Bet { CustomerId = 6, HorseId = 51, RaceId = 5, Stake = 300, Won = false });
            bets.Add(new Bet { CustomerId = 7, HorseId = 52, RaceId = 5, Stake = 300, Won = false });
            bets.Add(new Bet { CustomerId = 8, HorseId = 53, RaceId = 5, Stake = 300, Won = true });
            bets.Add(new Bet { CustomerId = 9, HorseId = 54, RaceId = 5, Stake = 100, Won = true });
            bets.Add(new Bet { CustomerId = 9, HorseId = 53, RaceId = 5, Stake = 100, Won = false });
            return bets;
        }
    }
}

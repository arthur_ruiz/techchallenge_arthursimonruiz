﻿using AutoMapper;

namespace RaceDayAPI.Tests.Models
{
    public static class TestConfig
    {
        private static bool isInitialized = false;

        public static void Initialize()
        {
            if (!isInitialized)
            {
                Mapper.Initialize(cfg => { });
                isInitialized = true;
            }
        }
    }
}

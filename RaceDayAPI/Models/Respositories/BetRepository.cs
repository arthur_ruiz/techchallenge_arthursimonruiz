﻿using Newtonsoft.Json;
using RaceDayAPI.Models.ServiceEntities;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace RaceDayAPI.Models.Respositories
{
    public class BetRepository: IBetRepository
    {

        private string _betServiceUrl;
        private HttpClient _client;

        public BetRepository()
        {
            _betServiceUrl = ConfigurationManager.AppSettings["betServiceUrl"];
            _client = new HttpClient();
        }

        public async Task<IEnumerable<Bet>> GetBets()
        {
            IEnumerable<Bet> bets = null;

            var response = await _client.GetAsync(_betServiceUrl);

            if (response.IsSuccessStatusCode)
            {
                var resultString = await response.Content.ReadAsStringAsync();
                bets = JsonConvert.DeserializeObject<IEnumerable<Bet>>(resultString);
            }

            return bets;
        }
    }
}
﻿using RaceDayAPI.Models.Respositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaceDayAPI.Tests.Models
{
    public class TestRepositories : IRepositories
    {
        public ICustomerRepository CustomerRepository { get; set; }
        public IBetRepository BetRepository { get; set; }
        public IRaceRepository RaceRepository { get; set; }

        public TestRepositories()
        {
            CustomerRepository = new TestCustomerRepository();
            BetRepository = new TestBetRepository();
            RaceRepository = new TestRaceRepository();
        }
    }
}

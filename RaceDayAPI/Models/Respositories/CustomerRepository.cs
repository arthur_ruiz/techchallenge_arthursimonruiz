﻿using Newtonsoft.Json;
using RaceDayAPI.Models.ServiceEntities;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace RaceDayAPI.Models.Respositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private string _customerServiceUrl;
        private HttpClient _client;

        public CustomerRepository()
        {
            _customerServiceUrl = ConfigurationManager.AppSettings["customerServiceUrl"];
            _client = new HttpClient();
        }

        public async Task<Customer> GetCustomer(int id)
        {
            var customers = await GetCustomers();

            if (customers == null)
            {
                return null;
            }

            return customers.SingleOrDefault(c => c.Id == id);
        }

        public async Task<IEnumerable<Customer>> GetCustomers()
        {
            IEnumerable<Customer> customers = null;

            var response = await _client.GetAsync(_customerServiceUrl);

            if (response.IsSuccessStatusCode)
            {
                var resultString = await response.Content.ReadAsStringAsync();
                customers = JsonConvert.DeserializeObject<IEnumerable<Customer>>(resultString);
            }

            return customers;
        }
    }
}
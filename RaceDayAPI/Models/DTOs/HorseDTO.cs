﻿namespace RaceDayAPI.Models.DTOs
{
    public class HorseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Odds { get; set; }

        public int TotalBets { get; set; }
        public decimal WinPayout { get; set; }
    }
}
﻿using RaceDayAPI.Models.Respositories;
using RaceDayAPI.Models.ServiceEntities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RaceDayAPI.Tests.Models
{
    public class TestCustomerRepository : ICustomerRepository
    {
        public async Task<Customer> GetCustomer(int id)
        {
            var customers = await GetCustomers();
            return customers.SingleOrDefault(c => c.Id == id);
        }

        public async Task<IEnumerable<Customer>> GetCustomers()
        {
            List<Customer> testCustomers = new List<Customer>();
            testCustomers.Add(new Customer { Id = 1, Name = "Rob" });
            testCustomers.Add(new Customer { Id = 2, Name = "Lachlan" });
            testCustomers.Add(new Customer { Id = 3, Name = "Mark" });
            testCustomers.Add(new Customer { Id = 4, Name = "Jared" });
            testCustomers.Add(new Customer { Id = 5, Name = "Peter" });
            testCustomers.Add(new Customer { Id = 6, Name = "John" });
            testCustomers.Add(new Customer { Id = 7, Name = "Doc" });
            testCustomers.Add(new Customer { Id = 8, Name = "Grumpy" });
            testCustomers.Add(new Customer { Id = 9, Name = "Happy" });
            testCustomers.Add(new Customer { Id = 10, Name = "Sleepy" });
            testCustomers.Add(new Customer { Id = 11, Name = "Bashful" });
            testCustomers.Add(new Customer { Id = 12, Name = "Sneezy" });
            testCustomers.Add(new Customer { Id = 13, Name = "Dopey" });
            return testCustomers;

        }
    }
}
